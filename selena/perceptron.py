#!/usr/bin/python3.6

from numpy import exp, array, random, dot


def neural_sigmoid(x):
    return 1/(1+exp(-x))

def neural_sigmoid_derivative(x):
    return (x*(1-x))

def neural_predict(weights, inputs):
    return neural_sigmoid(dot(inputs, weights))

def neural_train(weights, inputs, outputs, iterations):
    for iteration in range(iterations):
        output = neural_predict(weights, inputs)
        error = outputs - output
        adjustment = dot(inputs.T, error * neural_sigmoid_derivative(output))
        weights += adjustment

def main():
    print("hihihi")
    random.seed(1)
    weights = random.random((3,1)) * 10

    print('\nINIT\nRandom starting')
    print(weights)

    # my formula: 0x + 0y + z
    inputs = array([ [1,1,1],[0,0,0],[1,0,0],[0,1,0],[0,0,1],[1,1,0],[1,0,1],[0,1,1] ])
    outputs = array([[1,0,0,0,1,0,1,1]]).T
    neural_train(weights, inputs, outputs, 10000)

    print('\nCALCULATION\nNew weights:')
    print(weights)
    # how to make weight be possible to be bigger than 1?

    new_input = array([1,0,1])
    print('\nRESULT\nPredicting this: ', new_input)
    print(neural_predict(weights, new_input))

main()
