#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

# this is a classification perceptron
# with this type of perceptron you can split two datasets
# linearly separable

import numpy as np

# activation function
# give a probability of belonging to the dataset 1 or 0

def __sigmoid(x):
    return (1 / (1 + np.exp(-x)))

# the derivate is use here to do a gradient descent to make sure to do
# the correction in the right direction

def derivate_sigmoid(x):
    return (x*(1-x))

# in the training we calculate the error to do an adjustment on the weight
# a certain number of time

def train(entry_data, output_data, number_of_time, weights):
    adjustment = 0
    for i in range(number_of_time):
        output = predict(entry_data, weights)
        error = output_data - output
        adjustment = np.dot(entry_data.T, error * derivate_sigmoid(output))
        weights += adjustment
    return(weights)

# matrix product between the inputs and the weights

def predict(inputs, weights):
    return np.round ((__sigmoid(np.dot(inputs, weights))))


def perceptron():
    np.random.seed(1)
    weights = 2 * np.random.random((2,1)) - 1
    entry_data = np.array([[0,0], [1, 1], [1, 0], [0, 1], [-2, -1]])
    output_data = np.array([[1, 1, 1, 1, 0]]).T
    weights = train(entry_data, output_data, 1000, weights)
    prediction = predict(np.array([1, -2]), weights)
    print(prediction)

def _main_():
    perceptron()

_main_()
